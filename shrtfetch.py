# shrtfetch v 0.1
import argparse, platform, psutil, getpass, socket
from colorama import init, Fore, Style
from tabulate import tabulate

init()
version = "v0.1"

#parse args
parser = argparse.ArgumentParser()
parser.add_argument("--cpu", help="only show cpu info", action="store_true")
parser.add_argument("--os", help="only show os info", action="store_true")
parser.add_argument("--ip", help="only show ip addr", action="store_true")
args = parser.parse_args()

#colours
r = Fore.RED
b = Fore.BLUE
cc = Style.RESET_ALL

#print variables
architecture = platform.architecture()
uname = platform.uname()

cpu = uname.processor
os_system = uname.system
os_release = uname.release
os_architecture = architecture[0]
os_fix = os_system+" "+os_release+" "+os_architecture
mem = psutil.virtual_memory()
mem_total = mem.total/1024/1024/1024
host_name = socket.gethostname()
ip = socket.gethostbyname(host_name)
host_fix = getpass.getuser()+r+" @ "+cc+host_name
mem_fix = str(round(mem_total, 1))+" GB"

if args.cpu:
    print(cpu)
elif args.os:
    print(os_fix)
elif args.ip:
    print(ip)
else:
    print('\033[2J')
    header = [r+"SHRT FETCH "+cc, b+version+cc]

    data = [[b+"Host:"+cc, host_fix],
             [b+"Os:"+cc, os_fix],
             [b+"Cpu:"+cc, cpu],
             [b+"Ram:"+cc, mem_fix],
             [b+"Ip:"+cc, ip]]

    print(tabulate(data, header,  tablefmt="github"))